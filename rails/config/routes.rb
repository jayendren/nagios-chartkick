Rails.application.routes.draw do
  get "home/index"
  get "uptime/index"
  get "webapp/index"
  root to: "home#index"
end