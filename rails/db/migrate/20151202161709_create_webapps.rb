class CreateWebapps < ActiveRecord::Migration
  def change
    create_table :webapps do |t|
      t.string :name
      t.datetime :period
      t.float :availability

      t.timestamps
    end
  end
end
