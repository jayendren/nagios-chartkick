class CreateUptimes < ActiveRecord::Migration
  def change
    create_table :uptimes do |t|
      t.string :name
      t.datetime :period
      t.float :availability

      t.timestamps
    end
  end
end
