require 'net/https'
require 'nokogiri'
require 'zlib'

class Nagios_avail_dbseed

	@nagios_user             = "guest"
	@nagios_pass             = "guest"
	@nagios_web_timeout      = 30
  @nagios_web_url          = "http://10.33.8.125/prod/nagios/cgi-bin/avail.cgi?"

  $HUMANREADABLE = { 
    "12" => "Dec",
    "11" => "Nov",
    "10" => "Oct",
    "9"  => "Sept",
    "8"  => "Aug",
    "7"  => "Jul",
    "6"  => "Jun",
    "5"  => "May",
    "4"  => "Apr",
    "3"  => "Mar",
    "2"  => "Feb",
    "1"  => "Jan",
  }

  def self.integer_month(data)
    $HUMANREADABLE.each {|k,v|
      if data.to_s.match(/^#{k}$/)
        return v.chomp
      else
        puts "#{value} not found in map"
      end
    }
  end 

  def self.month_integer(data)
    $HUMANREADABLE.each {|k,v|
      if data.to_s.match(/^#{v}$/)
        return k.chomp
      else
        puts "#{value} not found in map"
      end
    }
  end   

  def self.web(url,username,pass,timeout = 60)
    uri                     = URI.parse(url)
    http                    = Net::HTTP.new(uri.host, uri.port,)
    http.use_ssl            = (uri.scheme == "https")
    http.verify_mode        = OpenSSL::SSL::VERIFY_NONE  
    http.read_timeout       = timeout           
    request                 = Net::HTTP::Get.new(uri.request_uri)
    request.initialize_http_header({"Accept-Enoding" => ""})
    #request.initialize_http_header({"Accept" => "*/*"})
    request.basic_auth(username,pass)

    begin
      response                = http.request(request)
      if response.code == "200"
        result = 1
        rcode  = response.code + " passed"
      elsif response.code == "302"
        result = 1
        rcode  = response.code + " passed"
      else
        result = 0
        rcode  = response.code + " failed"
      end
      rescue Timeout::Error
        result = 0
        rcode  = "Timeout::Error"
      rescue Errno::ETIMEDOUT
        result = 0
        rcode  = "Errno::ETIMEDOUT"
      rescue Errno::EHOSTUNREACH
        result = 0
        rcode  = "Errno::EHOSTUNREACH"
      rescue Errno::ECONNREFUSED
        result = 0
        rcode  = "Errno::ECONNREFUSED"
      rescue SocketError => e
        result = 0
        rcode  = "SocketError"
      #rescue Zlib::BufError => e
      #  rcode  = e
      #  puts e    
    end

    return response.body
    http.finish
  end

  def self.report(service,name,timeperiod,nagios_avail_dbseed_host,dbname)
		svc       = []
    avail     = []
    url       = @nagios_web_url + "host=" + nagios_avail_dbseed_host + "&service=" + service + "&timeperiod=" + timeperiod
    report    = Nokogiri::HTML((Nagios_avail_dbseed.web(url,@nagios_user,@nagios_pass,@nagios_web_timeout)))
    result    = report.xpath('/html/body/div[3]/table/tr[4]/td[4]').to_s.split("serviceOK")[1].to_s.split(">")[1].to_s.split("<")[0]

    #svc   << service
    #avail << result
    #return Hash[svc.zip(avail)]

    human_timestamp = timeperiod.split(/\=|\&/)
    smon            = human_timestamp[2]
    sday            = human_timestamp[4]
    syear           = human_timestamp[6]
    month           = Nagios_avail_dbseed.integer_month(smon)
    period          = "#{sday} #{month} #{syear}"
    avail           = result.to_s.split("%")[0]
    
    printf "%s\.create  name: \'%s\', period: \'%s\', availability: %s\n" % [dbname, name, period, avail]

  end

  def self.month_report(year,month,service,dbname)
    #if service.is_a?(Array)
    #  for index in 1..11
    #    smon=index
    #    emon=index+1
    #    service.each {|svc|
    #      avail_report = Nagios_avail_dbseed.report(svc[:service],svc[:name],"custom&smon=#{smon}&sday=1&syear=#{year}&emon=#{emon}&eday=1&eyear=#{year}",svc[:host],dbname)
    #    }
    #  end
    #  # December
    #  service.each {|svc|
    #    avail_report = Nagios_avail_dbseed.report(svc[:service],svc[:name],"custom&smon=12&sday=1&syear=#{year}&emon=12&eday=3&eyear=#{year}",svc[:host],dbname)
    #  }
    #end

    smon=month
    sday=1
    if smon == 12 # december is special
      emon=smon
      eday=31
    else
      emon=smon+1
      eday=1
    end
    service.each {|svc|
      avail_report = Nagios_avail_dbseed.report(svc[:service],svc[:name],"custom&smon=#{smon}&sday=#{sday}&syear=#{year}&emon=#{emon}&eday=#{eday}&eyear=#{year}",svc[:host],dbname)
    }
    
  end

end

## MAIN
# example: 
# ~/gitwork/nagios-chartkick/lib/nagios_avail_dbseed » pry nagios_avail_dbseed.rb 2015 6                                                                                                                 f4921003@PMBU49210033D1WL

year  = ARGV[0].to_i
month = ARGV[1].to_i

# webapp availabilty create db seed for 2015 availability - June to December
# June to November
# format:
#       Webapp.create  name: 'noop-loans', period: '01 dec 2015', availability: 99.623


Nagios_avail_dbseed.month_report(year,month,appsrvs,"Uptime")