# README

Scrape availability reports from nagiosweb and create better looking reports using chartkick

![](rails/demo/webapp_avail.png)

## Ruby version
```
ruby 2.1.5
```

## Database creation
```
createuser chartkick
createdb -O chartkick chartkick_production
createdb -O chartkick chartkick_test
createdb -O chartkick chartkick_development
rails g model Webapp name:string period:datetime availability:float
rails g model Uptime name:string period:datetime availability:float
```

## Database initialization
```
rake db:migrate
bin/nagios_avail_dbseed.rb 2015 6 > db/seeds.rb
rake db:seed
```

## How to run the test suite

TODO 

## Services (job queues, cache servers, search engines, etc.)

TODO

## Deployment instructions
```
rails new nagios-chartkick --database=postgresql
cd nagios-chartkick
echo "gem 'chartkick', '~> 1.2.4'\ngem 'groupdate', '~> 2.1.1'\ngem 'active_median', '~> 0.1.0'\ngem 'execjs'\n
gem 'therubyracer'" >> Gemfile
bundle install
```

## Configuration
```
rails g controller webapp index
rails g controller uptime index
rails g controller home index
```

## config/routes.rb
```
Rails.application.routes.draw do
  get "home/index"
  get "uptime/index"
  get "webapp/index"
  root to: "home#index"
end
```

## app/views/layouts/application.html.erb
```
<!DOCTYPE html>
<html>
<head>
  <title>Nagios Chartkick Availability Reports</title>
  
  <%= stylesheet_link_tag    'application', media: 'all' %>
  <%= javascript_include_tag 'application' %>
  <%= javascript_include_tag "http://www.google.com/jsapi", "chartkick" %>
  <%= stylesheet_link_tag    'http://yandex.st/bootstrap/3.1.1/css/bootstrap.min.css' %>
  <%= javascript_include_tag 'http://yandex.st/bootstrap/3.1.1/js/bootstrap.min.js' %>
  
  <%= csrf_meta_tags %>
</head>
<body>
  <div class="container">
    <%= yield %>
  </div>
</body>
</html>
```
## app/views/uptime/index.html.erb
```
<div class="row">
  <div class="col-xs-12">
    <h3>2015 PBC HOST UPTIME Availability</h3>
    <%=      
      line_chart  Uptime.pluck("name").uniq.map { |c| 
        { name: c, 
          data: @uptime.where(name: c).group_by_month(:period).maximum(:availability),
        }  
      },
      discrete: true,
      height:   "500px",
      min:      95, 
      max:      100,
      xtitle:   "Period", 
      ytitle:   "Availability",
      colors:   [
        "red", "green", "blue", "orange", "yellow", "black", "magenta", "teal", "turquoise", "pink", "cyan"
      ],
      library:  { 
        backgroundColor: "#FFF", 
        curveType:       "none", 
        pointSize:       5,
      }     
    %>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <h3>RAW DATA</h3>
    <%  Uptime.pluck("name","period","availability").each do |data| %>
      <p><%=  data %></p>
    <% end %>
      
  </div>
</div>
```

## app/views/webapp/index.html.erb
```
<div class="row">
  <div class="col-xs-12">
    <h3>2015 PBC WEBAPP Availability</h3>
    <%=      
      line_chart  Webapp.pluck("name").uniq.map { |c| 
        { name: c, 
          data: @webapp.where(name: c).group_by_month(:period).maximum(:availability),
        }  
      },
      discrete: true,
      height:   "500px",
      min:      95, 
      max:      100,
      xtitle:   "Period", 
      ytitle:   "Availability",
      colors:   [
        "red", "green", "blue", "orange", "yellow", "black", "magenta", "teal", "turquoise", "pink", "cyan"
      ],
      library:  { 
        backgroundColor: "#FFF", 
        curveType:       "none", 
        pointSize:       5,
      }     
    %>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <h3>RAW DATA</h3>
    <%  Webapp.pluck("name","period","availability").each do |data| %>
      <p><%=  data %></p>
    <% end %>
      
  </div>
</div>
```

## app/controllers/uptime_controller.rb
```
class UptimeController < ApplicationController
  def index
    @uptime = Uptime.all
  end
end
```

## app/controllers/webapp_controller.rb
```
class WebappController < ApplicationController
  def index
    @webapp = Webapp.all
  end
end
```
## config/initializers/chartkick.rb
```
Chartkick.options = {
  height: "300px",
  colors: ["#ff0000", "#00ff00", "#0000ff", "#ffff00", "#ff00ff", "0000ff"]
}
```
## Start rails server
```
bin/rails server
```
## Access railsapp
```
http://localhost:3000
```
