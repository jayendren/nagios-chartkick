#!/bin/bash

set -e

APP_DIR=/app
DBSETTINGS=$APP_DIR/config/database.yml

if [[ ! -f ${DBSETTINGS}.bak ]]; then
    if [ -z $PG_HOST ]; then
        echo Please provide a host for the 'pg database' via the PG_HOST enviroment variable.
        exit 1
    fi

    sed -i.bak -e "s!host: localhost!host: $PG_HOST!g" $DBSETTINGS 
fi

unset PG_HOST

cd $APP_DIR && \
bundle install && \
bin/rake db:migrate && \
bin/rake db:seed && \
bin/rails server