FROM ruby:2.2
MAINTAINER jayendren <jayendren@gmail.com>

# ENVARS
ENV TIME_ZONE Africa/Johannesburg

# INSTALL
RUN apt-get update && \    
    apt-get install -yq --no-install-recommends \
    build-essential nodejs
    
# POST INSTALL
RUN cp /usr/share/zoneinfo/${TIME_ZONE} /etc/localtime
ADD src/start.sh /start.sh
RUN chmod 775 /start.sh

# RAILS
RUN mkdir -p /app 
WORKDIR /app
ADD rails /app

# CLEAN UP
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# PORTS
EXPOSE 3000

# CMD
#ENTRYPOINT ["/start.sh"]